/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.realcorporation.tp17012018;


import fr.realcorporation.tp17012018.Place.Type;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author cdacruz
 */
public class Main {
    
    /* CREATION DU LOGGER */
    final static Logger logger = Logger.getLogger("Main");
    
    /* RECUPERATION DE LA VALEUR DE L'ARGUMENT */
    public static void main(String[] testArg){
        logger.log(Level.SEVERE, "La valeur de l'argument que l'on récupère est de "+testArg[0]);
//        
//        //On demande à l'utilisateur de taper un nombre avec la classe Scanner
//	Scanner sc = new Scanner(System.in);
//        String str = "";
//        //Boucle do while tant que l'utilisateur ne saisi pas autre chose que des lettres 
//        //Ou un nombre/chiffre négatif
//        do{
//        System.out.println("Veuillez saisir un nombre ou taper \"stop\" pour quitter, c'est à vous : ");
//         str = sc.nextLine();
//        if(isInteger(str) && Integer.parseInt(str) > 0){
//            exo1(str);
//        }
//        //Ou lorsque il tape "stop"
//        }while(!"stop".equals(str));
        
//        tryAddress();
//        tryPlace();
        tryPerson();
	}
    
    /* CALCUL DE LA FACTORIELLE */ 
    
    public static int exo1 (String value){
        int a = Integer.parseInt(value);
		int fact = 1;
		for(int i = a; i >= 1; i--){
			fact = fact*i;
		}
		logger.log(Level.SEVERE, "La factoriel de "+value+" est égale à "+fact);
                return fact;
    }
   
    /* CONTROLE SI LE(S) CARACTERE(S) RENTRÉ(S) EST(SONT) UN INT */
    private static boolean isInteger(String s) {
       boolean isValid = true;
       try{ Integer.parseInt(s); }
       catch(NumberFormatException nfe){ isValid = false; }
       return isValid;
   }
    
    // Exercice 3
    
    public static void tryAddress(){
      Address addressTest = new Address("14", "Rue Ledru", "Rollin", "36000", "Châteauroux", "France", "Flemme d'en mettre une", "Pareil");
        logger.log(Level.SEVERE, addressTest.toString());
    }
    
    // Exercice 4 
    
    public static void tryPlace(){
      Address addressTest = new Address("14", "Rue Ledru", "Rollin", "36000", "Châteauroux", "France", "Flemme d'en mettre une", "Pareil");
            Place placeTest = new Place("SushiMaster", addressTest, Type.Restaurant, 13);
        logger.log(Level.SEVERE, placeTest.toString());
    }
    
    // Exercice 5
    public static void tryPerson(){
    Person personTest = new Person("Christophe", "Da Cruz", "22", "Montreuil");
    logger.log(Level.SEVERE, personTest.toString());
    }
}
