/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.realcorporation.tp17012018;

/**
 *
 * @author cdacruz
 */

/* CLASSE DE L'EXERCICE 4 */

public class Place {
    
    String name;
    Address adresse;
    Type type;
    int mark;
    
    public enum Type{
        École, Cinéma, Théâtre, Restaurant
    }
    
    public Place(String name, Address adresse, Type type, int mark){
        this.name = name;
        this.adresse = adresse;
        this.type = type;
        this.mark= mark;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Address getAdresse() {
        return adresse;
    }

    public void setAdresse(Address adresse) {
        this.adresse = adresse;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public int getMark() {
        return mark;
    }

    public void setMark(int mark) {
        this.mark = mark;
    }
    
    
    
    @Override
    public String toString(){
        return "Le lieu est "+name+" situé à "+adresse+". Ce lieu est un "+type+" qui a une note globale de "+mark;
        
    }
}

