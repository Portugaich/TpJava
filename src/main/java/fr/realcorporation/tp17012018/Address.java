/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.realcorporation.tp17012018;

/**
 *
 * @author cdacruz
 */

/* CLASSE DE L'EXERCICE 3 */

public class Address {
    
    String streetNumber;
    String streetName;
    String streetName2;
    String zipCode;
    String city;
    String country;
    String longitude;
    String latitude;
    
    public Address(String streetNumber, String streetName, String streetName2, String zipCode, String city, String country, String latitude, String longitude){
        this.streetNumber = streetNumber;
        this.streetName= streetName;
        this.streetName2= streetName2;
        this.zipCode = zipCode;
        this.city = city;
        this.country = country;
        this.longitude = longitude;
        this.latitude = latitude;
    }
    
    public Address(String streetNumber, String streetName, String streetName2, String zipCode, String city, String country){
        this.streetNumber = streetNumber;
        this.streetName= streetName;
        this.streetName2= streetName2;
        this.zipCode = zipCode;
        this.city = city;
        this.country = country;
    }

    public String getStreetNumber() {
        return streetNumber;
    }

    public void setStreetNumber(String streetNumber) {
        this.streetNumber = streetNumber;
    }

    public String getStreetName() {
        return streetName;
    }

    public void setStreetName(String streetName) {
        this.streetName = streetName;
    }

    public String getStreetName2() {
        return streetName2;
    }

    public void setStreetName2(String streetName2) {
        this.streetName2 = streetName2;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }
    
    
    
    @Override
    public String toString(){
        return "Address is "+streetNumber+" "+streetName+" "+streetName2+", "+zipCode+" "+city+" "+" "+country+". Gps location = Lat : "+latitude+"|| Lon : "+longitude;
        
    }
    
}
