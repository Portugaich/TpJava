/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.realcorporation.tp17012018;

/**
 *
 * @author cdacruz
 */

/* CLASSE DE L'EXERCICE 6 */

public class Student extends Person{
    
    public Student(String lastname, String firstname, String age, String placeOfBirth) {
        super(lastname, firstname, age, placeOfBirth);
    }
    
    String classe;

    public Student(String classe, String lastname, String firstname, String age, String placeOfBirth) {
        super(lastname, firstname, age, placeOfBirth);
        this.classe = classe;
    }

    public String getClasse() {
        return classe;
    }

    public void setClasse(String classe) {
        this.classe = classe;
    }

    @Override
    public String toString() {
        return "Student{" + "classe=" + classe + '}';
    }
    
    
}
