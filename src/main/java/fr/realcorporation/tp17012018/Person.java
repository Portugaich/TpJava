/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.realcorporation.tp17012018;

/**
 *
 * @author cdacruz
 */

/* CLASSE DE L'EXERCICE 5 */

public class Person {
    
    String lastname;
    String firstname; 
    String age;
    String placeOfBirth;

    public Person(String lastname, String firstname, String age, String placeOfBirth) {
        this.lastname = lastname;
        this.firstname = firstname;
        this.age = age;
        this.placeOfBirth = placeOfBirth;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getPlaceOfBirth() {
        return placeOfBirth;
    }

    public void setPlaceOfBirth(String placeOfBirth) {
        this.placeOfBirth = placeOfBirth;
    }

    @Override
    public String toString() {
        return "Person{" + "lastname=" + lastname + ", firstname=" + firstname + ", age=" + age + ", placeOfBirth=" + placeOfBirth + '}';
    }

    

}
