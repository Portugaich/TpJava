/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.realcorporation.tp17012018;

import java.util.List;

/**
 *
 * @author cdacruz
 */

/* CLASSE DE L'EXERCICE 5 */

public class Teacher extends Person{
    
    public Teacher(String lastname, String firstname, String age, String placeOfBirth) {
        super(lastname, firstname, age, placeOfBirth);
    }
    
    List<String> cours;

    public Teacher(List<String> cours, String lastname, String firstname, String age, String placeOfBirth) {
        super(lastname, firstname, age, placeOfBirth);
        this.cours = cours;
    }

    public List<String> getCours() {
        return cours;
    }

    public void setCours(List<String> cours) {
        this.cours = cours;
    }

    @Override
    public String toString() {
        return "Teacher{" + "cours=" + cours + '}';
    }
    
    
    
}
